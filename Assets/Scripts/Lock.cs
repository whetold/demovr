﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;
using VRTK.UnityEventHelper;

public class Lock : MonoBehaviour
{
	[SerializeField] private Text _numberDisplay;
	[SerializeField] private VRTK_Button_UnityEvents[] _padButtons;
	[SerializeField] private string _password;

	private string NumberDisplayText
	{
		get { return _numberDisplay.text; }
		set { _numberDisplay.text = value; }
	}

	public event Action EnteredValidPassword;

	private void Start()
	{
		for (var i = 0; i < _padButtons.Length; ++i)
		{
			var currentButtonNumber = i + 1;
			_padButtons[i].OnPushed.AddListener((s, e) => OnButtonPushed(currentButtonNumber));
		}
	}

	private void OnButtonPushed(int buttonIndex)
	{
		NumberDisplayText = string.Concat(_numberDisplay.text, buttonIndex.ToString());
		CheckPassword();
	}

	private void CheckPassword()
	{
		if (NumberDisplayText.Length < _password.Length) return;

		if (NumberDisplayText == _password)
		{
			EnteredValidPassword?.Invoke();
			NumberDisplayText = "ACCEPTED";
			_padButtons.ForEach(_ => _.OnPushed.RemoveAllListeners());

			Door.Instance.CompleteCode();
			return;
		}

		NumberDisplayText = string.Empty;
	}
}