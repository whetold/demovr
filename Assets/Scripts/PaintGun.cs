﻿using UnityEngine;
using VRTK;
using VRTK.UnityEventHelper;

public class PaintGun : MonoBehaviour
{
	private PaintBullet _currentPaintBullet;

	[SerializeField] private VRTK_InteractableObject_UnityEvents _interactableObjectEvents;
	[SerializeField] private Transform _muzzleTransform;
	[SerializeField] private VRTK_SnapDropZone_UnityEvents _snapDropZoneEvents;

	private void Start()
	{
		_interactableObjectEvents.OnUse.AddListener(Shoot);
		_snapDropZoneEvents.OnObjectSnappedToDropZone.AddListener(AttachPaintBullet);
		_snapDropZoneEvents.OnObjectUnsnappedFromDropZone.AddListener(DeattachPaintBullet);
	}

	private void DeattachPaintBullet(object arg0, SnapDropZoneEventArgs arg1)
	{
		_currentPaintBullet = null;
	}

	private void AttachPaintBullet(object arg0, SnapDropZoneEventArgs arg1)
	{
		_currentPaintBullet = arg1.snappedObject.GetComponent<PaintBullet>();
	}

	private void Shoot(object arg0, InteractableObjectEventArgs arg1)
	{
		if (_currentPaintBullet == null) return;

		int layerMask = LayerMask.GetMask("Paintable");
		var fwd = _muzzleTransform.TransformDirection(Vector3.forward);

		RaycastHit raycastHit;
		if (Physics.Raycast(_muzzleTransform.position, fwd, out raycastHit, Mathf.Infinity, layerMask))
			raycastHit.transform.GetComponent<MeshPaint>().ItemColor = _currentPaintBullet.BulletColor;
	}
}