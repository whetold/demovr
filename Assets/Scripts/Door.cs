﻿using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
	public static Door Instance;

	[SerializeField] private Transform _doorMesh;
	[SerializeField] private MeshPaint _lightColor;

	private bool _codeCompleted;
	private bool _lightColorCompleted;
	private Coroutine _coroutine;

	public void CompleteCode()
	{
		_codeCompleted = true;
		OpenDoors();
	}

	private void Awake()
	{
		Instance = this;
		_lightColor.OnColorChanged += OnColorChanged;
	}

	private void OnColorChanged(Color color)
	{
		_lightColorCompleted = color == Color.green;
		OpenDoors();
	}

	private void OpenDoors()
	{
		if (_codeCompleted && _lightColorCompleted && _coroutine == null)
		{
			_coroutine = StartCoroutine(AnimateDoors());
		}
	}

	private IEnumerator AnimateDoors()
	{
		while (true)
		{
			_doorMesh.Translate(Vector3.up * Time.deltaTime, Space.World);
			yield return null;
		}
	}
}