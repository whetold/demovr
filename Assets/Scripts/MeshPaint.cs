﻿using System;
using UnityEngine;

public class MeshPaint : MonoBehaviour
{
	private Color _color;
	[SerializeField] private MeshRenderer[] _meshRenderers;

	public Action<Color> OnColorChanged;

	public Color ItemColor
	{
		get { return _color; }
		set
		{
			if (_color != value)
			{
				_color = value;
				foreach (var meshRenderer in _meshRenderers) meshRenderer.material.color = _color;
				OnColorChanged?.Invoke(_color);
			}
		}
	}

	private void Start()
	{
		if (_meshRenderers.Length == 0) _meshRenderers = GetComponentsInChildren<MeshRenderer>(true);
	}
}