﻿using UnityEngine;

public class PaintBullet : MonoBehaviour
{
	[SerializeField] private Color _color;
	[SerializeField] private MeshRenderer _meshRenderer;

	public Color BulletColor
	{
		get { return _color; }

		set
		{
			if (_color != null)
			{
				_color = value;
				_meshRenderer.material.color = _color;
			}
		}
	}

	private void Awake()
	{
		BulletColor = _color;
	}
}