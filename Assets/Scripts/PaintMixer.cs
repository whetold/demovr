﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRTK;
using VRTK.UnityEventHelper;

public class PaintMixer : MonoBehaviour
{
	private readonly List<PaintBullet> _bullets = new List<PaintBullet>();
	[SerializeField] private PaintBullet _bulletPrefab;
	[SerializeField] private VRTK_Button_UnityEvents _buttonEvents;
	[SerializeField] private VRTK_SnapDropZone_UnityEvents[] _snapDropZoneEvents;
	[SerializeField] private Transform _spawnPosition;

	private void Start()
	{
		_buttonEvents.OnPushed.AddListener((s, e) => OnButtonPushed());

		foreach (var snapzone in _snapDropZoneEvents)
		{
			snapzone.OnObjectSnappedToDropZone.AddListener(AttachPaintBullet);
			snapzone.OnObjectUnsnappedFromDropZone.AddListener(DeattachPaintBullet);
		}
	}

	private void OnButtonPushed()
	{
		if (_bullets.Count >= 2) MixCurrentPaint();
	}

	private void MixCurrentPaint()
	{
		if (_bullets.Any(_ => _.BulletColor == Color.yellow) && _bullets.Any(_ => _.BulletColor == Color.blue))
		{
			SpawnBullet(Color.green);
		}

		if (_bullets.Any(_ => _.BulletColor == Color.green) && _bullets.Any(_ => _.BulletColor == Color.blue))
		{
			SpawnBullet(Color.cyan);
		}

		if (_bullets.Any(_ => _.BulletColor == Color.red) && _bullets.Any(_ => _.BulletColor == Color.blue))
		{
			SpawnBullet(Color.magenta);
		}

		if (_bullets.Any(_ => _.BulletColor == Color.red) && _bullets.Any(_ => _.BulletColor == Color.green))
		{
			SpawnBullet(Color.yellow);
		}
	}

	private void SpawnBullet(Color color)
	{
		var newBullet = Instantiate(_bulletPrefab);

		newBullet.transform.position = _spawnPosition.position;
		newBullet.BulletColor = color;
	}

	private void DeattachPaintBullet(object arg0, SnapDropZoneEventArgs arg1)
	{
		var bullet = _bullets.FirstOrDefault(_ => _ == arg1.snappedObject.GetComponent<PaintBullet>());
		if (bullet != null) _bullets.Remove(bullet);
	}

	private void AttachPaintBullet(object arg0, SnapDropZoneEventArgs arg1)
	{
		_bullets.Add(arg1.snappedObject.GetComponent<PaintBullet>());
	}
}